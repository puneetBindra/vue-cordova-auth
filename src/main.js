import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'mdbvue/build/css/mdb.css'
import platform from 'platform'

Vue.config.productionTip = true;
Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(platform)

document.addEventListener("deviceready", () => {
  sessionStorage.setItem('token', 'intent')
  if (platform.name === 'Chrome') {
    axios.defaults.baseURL = 'https://vm8:50114'
    init()
  } else {
    axios.defaults.baseURL = 'https://192.168.1.208:50114'
    window.plugins.intent.getCordovaIntent(function (Intent) {
      var query = (Intent.data)
      if(Intent.data){
        var query1 = query.substring(query.indexOf('=') + 1 )
        sessionStorage.setItem('token', query1)
      }
      init()
    }, function () {
      console.log('Error');
    });
  }
});

const init = () => {
  new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
  });
};

const isCordovaApp = (typeof window.cordova !== "undefined");
if (!isCordovaApp){
  document.dispatchEvent(new CustomEvent("deviceready", {}));
}
