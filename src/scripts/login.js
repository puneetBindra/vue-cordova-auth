import {
  Container,
  Row,
  Column,
  Navbar,
  NavbarItem,
  NavbarNav,
  NavbarCollapse,
  ViewWrapper,
  MdMask,
  Btn,
  Card,
  CardBody,
  MdInput,
  Fa
} from 'mdbvue'

import qs from 'qs'

export default {
  name: 'ClassicFormPage',
  components: {
    Container,
    Row,
    Column,
    Navbar,
    NavbarItem,
    NavbarNav,
    NavbarCollapse,
    ViewWrapper,
    MdMask,
    Btn,
    Card,
    CardBody,
    MdInput,
    Fa
  },
  data () {
    return {
      active: {
        0: false
      },
      form: {
        email: '',
        password: ''
      }
    }
  },
  methods: {
    toggleDropdown (index) {
      for (let i = 0; i < Object.keys(this.active).length; i++) {
        if (index !== i) {
          this.active[i] = false
        }
      }
      this.active[index] = !this.active[index]
    },
    allDropdownsClose (target) {
      for (let i = 0; i < Object.keys(this.active).length; i++) {
        this.active[i] = false
      }
    },
    onClick (e) {
      let parent = e.target
      let body = document.getElementsByTagName('body')[0]
      while (parent !== body) {
        if (parent.classList.contains('dropdown') || parent.classList.contains('btn-group')) {
          return
        }
        parent = parent.parentNode
      }
      this.allDropdownsClose(e.target)
    },
    Submit () {
      this.axios.post('/v18-2/auth/mac/new', qs.stringify({
        'payload': {
          'path': '/auth/login/email/'
        }
      })).then(response => {
        const auth = {
          headers: {
            'Authorization': response.data.payload.macToken
          }
        }
        this.axios.post('/v18-2/auth/login/email', qs.stringify({
          'payload': {
            'email': this.form.email,
            'password': this.form.password
          }
        }), auth).then(response1 => {
          alert(response1.data.payload.accessToken)
        }, function (error) {
          console.log(JSON.stringify(error))
        })
      }, function (error) {
        console.log(JSON.stringify(error))
      })
    }
  },
  mounted () {
    document.addEventListener('click', this.onClick)
  },
  destroyed () {
    document.removeEventListener('click', this.onClick)
  }
}
