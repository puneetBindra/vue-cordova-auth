import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import Signup from '@/components/signup'
import Homepage from '@/components/homepage'
import Forgotpassword from '@/components/forgotpassword'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '',
      name: 'Login',
      components: {default:Login, a:Homepage}
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/homepage',
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '/forgotpassword',
      name: 'Forgotpassword',
      component: Forgotpassword
    }
  ]
})

export default router
