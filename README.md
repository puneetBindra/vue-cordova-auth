# vuejs-cordova-auth

Vue-Cordova app to login,signup,confirm-token & open android app via email confirmation.

## Build Setup
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production to run in Cordova
npm run '{platform}'
